import React, { useState, useEffect } from 'react'
import EditPost from './EditPost'
import { getById } from '../../services/httpRequests'
import { useParams } from 'react-router-dom';

export default function EditPostContainer() {
    const {id} = useParams();
    const [post, setPost] = useState({});

    useEffect(() => {
        const getData = async () => {
            const response = await getById(id);
            setPost(response.data)
            console.log(response.data);
            console.log(post)
        }
        getData();
    }, [id, post])

    return (
        <>
            {id<=100 ?
                <div id="editPostContainer">
                    <EditPost 
                        id={post.id}
                        title={post.title}
                        body={post.body}
                    />
                </div>
            :
                <div id="notFound">
                        <h5>Post not found</h5>
                </div>
            }
        </>
    )
}
