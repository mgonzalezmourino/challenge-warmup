import React from 'react'
import TextInput from './TextInput';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom'
import { Button } from 'react-bootstrap'

export default function SearchIdForm() {
    const history = useHistory();

    return (
        <div id="searchPostContainer">
            <h5>Edit post by ID:</h5>
            <div>
                <Formik
                    initialValues={{
                        postId: '',
                    }}
                    validationSchema={Yup.object({
                        postId: Yup.number()
                        .integer()
                        .default(0)
                        .required(<span>Required</span>)
                        .positive()
                    })}
                    onSubmit={(values, { setSubmitting }) => {
                        setTimeout(() => {
                            console.log(JSON.stringify(values, null, 2));
                            setSubmitting(false);
                            history.push(`/edit/${values.postId}`)
                        }, 400);
                    }}
                >
                    <Form id="searchBox">
                        <TextInput 
                            label=""
                            name="postId"
                            type="text"
                            placeholder="Enter post ID"
                        /> {' '}
                        <Button variant="success" bg="info" type="submit" id="searchButton">
                            <span className="material-icons xs-18">search</span>
                        </Button>
                    </Form>
                </Formik>
            </div>
        </div>
    )
}
