import React from 'react'
import { useField } from 'formik'

export default function TextInput({ label, ...props }) {
    const [field, meta] = useField(props);

    return (
        <div className="text_input">
            <label htmlFor={props.id || props.name} >{label}</label>
                {
                props.textarea ?
                    <div>
                        <textarea className="textarea_input" {...field} {...props} />
                        {meta.touched && meta.error ? (
                            <div className="error">{meta.error}</div>
                        ) : null}
                    </div>
                :
                    <div>
                        <input {...field} {...props} />
                        {meta.touched && meta.error ? (
                            <div className="error">{meta.error}</div>
                        ) : null}
                    </div>
                }
        </div>
    );
}