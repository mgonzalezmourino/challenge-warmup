import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom'
import { editById } from '../../services/httpRequests'
import TextInput from './TextInput';
import { Button } from 'react-bootstrap'

export default function EditPost(props) {
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const handleClickLoad = () => setLoading(true);
console.log(props.title)
    return (
                <div>
                    <h5>Edit post #{props.id}:</h5>
                    <Formik
                        initialValues={{
                            title: '',
                            body: ''
                        }}
                        validationSchema={Yup.object({
                            title: Yup.string()
                                .min(1, <span>Must have at least 1 character</span>)
                                .required(<span>Required</span>),
                            body: Yup.string()
                                .min(1, <span>Must have at least 1 character</span>)
                                .required(<span>Required</span>)
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setTimeout(() => {
                                handleClickLoad();
                                console.log(JSON.stringify(values, null, 2));
                                editById(props.id, 
                                    {
                                        title: values.title,
                                        body: values.body,
                                    }
                                );
                                setSubmitting(false);
                                alert("Post edited succesfully")
                                history.push('/');
                            }, 400);
                        }}
                    >
                        <Form>
                            <div>
                                <TextInput 
                                    label="Title: "
                                    name="title"
                                    type="text"
                                    placeholder="Edit Title"
                                />
                                <p>Unedited: {props.title}</p>
                            </div>
                            <div>
                                <TextInput 
                                    label="Body: "
                                    name="body"
                                    type="text"
                                    placeholder="Edit Body"
                                    textarea
                                />
                                <p>Unedited: {props.body}</p>
                            </div>
                            <Button 
                                variant="success"
                                bg="success" 
                                type="submit"
                                disabled={loading}
                            >
                                {loading ? 'Loading…' : 'Edit Post'}
                            </Button>
                        </Form>
                    </Formik>
                </div>
    )
}
