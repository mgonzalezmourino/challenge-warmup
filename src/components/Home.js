import React from 'react'
import HomePresentation from './home/HomePresentation'
import PostsListContainer from './home/PostsListContainer'
import { Container } from 'react-bootstrap'

export default function Home() {
    return (
        <Container>
            <HomePresentation />
            <PostsListContainer />
        </Container>
    )
}
