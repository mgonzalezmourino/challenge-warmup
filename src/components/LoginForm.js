import React, { useState } from 'react'
import TextInput from '../components/edit/TextInput'
import { Button } from 'react-bootstrap'
import { Formik, Form } from 'formik'
import * as Yup from 'yup';
import { login } from '../services/login'

export default function LoginForm() {
    const [enter, setEnter] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const handleClickLoad = () => setLoading(true);

    return (
        <div id="loginFormContainer">
            <div id="loginForm">
                    <h5>Login in our Blog</h5>
                    <Formik
                        initialValues={{
                            email: '',
                            password: '',
                        }}
                        validationSchema={Yup.object({
                            email: Yup.string()
                                .email(<span>Invalid adress</span>)
                                .required(<span>Required</span>),
                            password: Yup.string()
                                .min(4, <span>Must have more than 4 characters</span>)
                                .required(<span>Required</span>)
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setTimeout(() => {
                                handleClickLoad();
                                console.log(JSON.stringify(values, null, 2));
                                login(values.email, values.password);
                                setEnter(true);
                                setSubmitting(false);
                            }, 400);
                        }}
                    >
                        <Form>
                            <div className="my-2">
                                <TextInput 
                                    label="Email: "
                                    name="email"
                                    type="text"
                                    placeholder="challenge@alkemy.org"
                                />
                            </div>
                            <div className="my-2">
                                <TextInput 
                                    label="Password: "
                                    name="password"
                                    type="password"
                                    placeholder="react"
                                />
                            </div>
                            <Button 
                                variant="primary" 
                                bg="primary" 
                                type="submit"
                                disabled={isLoading}
                            >
                                {isLoading ? 'Loading…' : 'Login'}
                            </Button>
                            {enter &&
                                <Button 
                                variant="success" 
                                bg="success" 
                                type="submit"
                                onClick={() => window.location.reload()}
                                >
                                    Enter
                                </Button>
                            }
                        </Form>
                    </Formik>
            </div>
        </div>
    )
}