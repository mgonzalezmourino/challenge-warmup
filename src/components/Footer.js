import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'

export default function Footer() {
    return (
        <div id="footer">
            <Container>
                <Row>
                    <Col xs={4}>
                        <h4>Links</h4>
                        <ul>
                            <li>Home</li>
                            <li>Edit</li>
                        </ul>
                    </Col>
                    <Col xs={4}>
                        <h4>Posts</h4>
                        <ul>
                            <li>Post 1</li>
                            <li>Post 2</li>
                            <li>Post 3</li>
                            <li>Post 4</li>
                        </ul>
                    </Col>
                    <Col xs={4}>
                        <h4>Follow us</h4>
                        <ul>
                            <li>Facebook</li>
                            <li>Instagram</li>
                            <li>Twitter</li>
                            <li>Linkedin</li>
                        </ul>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
