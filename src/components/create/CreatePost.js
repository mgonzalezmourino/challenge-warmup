import React from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom'
import { create } from '../../services/httpRequests'
import TextInput from '../edit/TextInput';
import { Button } from 'react-bootstrap'

export default function CreatePost() {
    const history = useHistory();
    const id = Math.floor(Math.random()*1000);

    return (
                <div id="editPostContainer">
                    <h5>Create Post:</h5>
                    <Formik
                        initialValues={{
                            userId:  '',
                            title: '',
                            body: ''
                        }}
                        validationSchema={Yup.object({
                            userId: Yup.number()
                                .integer()
                                .default(11)
                                .required(<span>Required</span>)
                                .positive(),
                            title: Yup.string()
                                .min(1, <span>Must have at least 1 character</span>)
                                .required(<span>Required</span>),
                            body: Yup.string()
                                .min(1, <span>Must have at least 1 character</span>)
                                .required(<span>Required</span>)
                        })}
                        onSubmit={(values, { setSubmitting }) => {
                            setTimeout(() => {
                                console.log(JSON.stringify(values, null, 2));
                                create({
                                        id: id,
                                        userId: values.userId,
                                        title: values.title,
                                        body: values.body,
                                    }
                                );
                                setSubmitting(false);
                                alert("Post created succesfully")
                                history.push('/');
                            }, 400);
                        }}
                    >
                        <Form>
                            <div>
                                <TextInput 
                                    label="User ID: "
                                    name="userId"
                                    type="number"
                                    placeholder="Enter user Id"
                                />
                            </div>
                            <div>
                                <TextInput 
                                    label="Title: "
                                    name="title"
                                    type="text"
                                    placeholder=""
                                />
                            </div>
                            <div>
                                <TextInput 
                                    label="Body: "
                                    name="body"
                                    type="text"
                                    placeholder=""
                                    textarea
                                />
                            </div>
                            <Button 
                                variant="success"
                                bg="success" 
                                type="submit"
                            >
                                Create post
                            </Button>
                        </Form>
                    </Formik>
                </div>
    )
}
