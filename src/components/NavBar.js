import React from 'react'
import { Navbar,
        Container,
        Nav
} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function NavBar() {
    return (
        <Navbar bg="success" expand="lg">
            <Container>
                <Navbar.Brand href="/">
                    <img 
                        alt="Blog logo"
                        src="https://images.vexels.com/media/users/3/188258/isolated/lists/a21b4bcdaeecaec7ff9c14c7814da10d-letra-mayuscula-occidental-trazo-de-sombra-b.png"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{' '}
                    Blog
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarResponsive" />
                <Navbar.Collapse id="navbarResponsive">
                    <Nav className="ms-auto" id="navContainer">
                        <Link to='/'>
                            Home
                        </Link>
                        <Link to="/edit">
                            Edit post
                        </Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
