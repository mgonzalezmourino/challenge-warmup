import React from 'react'
import { Container } from 'react-bootstrap'

export default function PostDetail(props) {
    return (
        <Container>
            <div>
                <h3>{props.title}</h3>
                <p id="detailSubtitle">Published by "User {props.userId}" on {Date()}</p>
                <p id="detailBody">{props.body}</p>
            </div>
        </Container>
    )
}
