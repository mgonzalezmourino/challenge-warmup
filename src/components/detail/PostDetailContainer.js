import React, { useEffect, useState } from 'react'
import PostDetail from './PostDetail'
import { getById } from '../../services/httpRequests'
import { useParams } from 'react-router-dom'

export default function PostDetailContainer() {
    const {id} = useParams();
    const [detail, setDetail] = useState({});

    useEffect(() => {
        const getDetailData = async () => {
            const response = await getById(id);
            setDetail(response.data)
            console.log(response.data);
            console.log(detail)
        }
        getDetailData();
    }, [id, detail])

    return (
        <div id="detailContainer">
            {detail.title ?
                <PostDetail
                title={detail.title}
                userId={detail.userId}
                body={detail.body}
                />
            :
                <div id="notFound">
                    <h5>Post not found</h5>
                </div>
            }
            
        </div>
    )
}
