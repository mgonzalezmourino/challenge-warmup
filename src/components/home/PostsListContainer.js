import React, { useEffect, useState } from 'react'
import PostsList from './PostsList'
import { getAll } from '../../services/httpRequests'
import { Spinner } from 'react-bootstrap';

export default function PostsListContainer() {
    const [posts, setPosts] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const getPosts = async () => {
            setLoading(true)
            const response = await getAll();
            setPosts(response.data);
            console.log(response.data);
            setLoading(false);
        };
        getPosts();
    }, [])

    return (
        <>{
            loading ? 
            <div id="loading">
                <Spinner id="spinner" animation="border" variant="success"/> 
            </div>
            : 
            <PostsList posts={posts}/>
        }
        </>
    )
}
