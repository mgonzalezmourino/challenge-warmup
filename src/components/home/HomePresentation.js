import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function HomePresentation() {
    return (
        <div className="home_presentation_container">
            <h2>Blog</h2>
            <p>
                The ideal platform to tell the world how you think. Now, only a click away!
            </p>
            <Link to='/create/'>
                <Button 
                variant="outline-success"
                size="sm"
                >
                    Create Post
                </Button>
            </Link>
        </div>
    )
}
