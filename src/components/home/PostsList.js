import React from 'react'
import PostCard from './PostCard'
import { Row, Col } from 'react-bootstrap'

export default function PostsList({ posts }) {
    return (
        <Row>
            {
                posts.map((item) => {
                    return(
                        <Col
                            key={item.id}
                            lg={4}
                            md={4}
                            sm={6}
                            xs={12}
                        >
                            <PostCard
                                id={item.id}    
                                userId={item.userId}
                                title={item.title}
                            />
                        </Col>
                    )
                })
            }
        </Row>
    )
}
