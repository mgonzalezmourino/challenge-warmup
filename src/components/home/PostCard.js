import React from 'react'
import { deleteById } from '../../services/httpRequests'
import { Card, Button } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'

export default function PostCard(props) {
    const history = useHistory();

    const handleDelete = (id) => {
        deleteById(id);
        alert("Post deleted")
        history.push('/');
    }

    return (
        <Card className="my-2">
            { props.id < 8 && <Card.Header className="card_header">New</Card.Header>}
            <Card.Body>
                <Card.Title className="card_title">{props.title}</Card.Title>
                <Card.Text></Card.Text>
                <Link to={`/detail/${props.id}`}>
                    <Button variant="primary" size="sm">See more</Button>
                </Link>{' '}
                <Link to={`/edit/${props.id}`}>
                    <Button variant="outline-success" size="sm">Edit</Button>
                </Link>{' '}
                <Button variant="outline-danger" size="sm" onClick={() => {handleDelete(props.id)}}>Delete</Button>
            </Card.Body>
        </Card>
    )
}
