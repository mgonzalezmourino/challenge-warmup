import http from "./httpCommon"

const getAll = async () => {
    const response = await http.get("/posts");
    return response;
}

const getById = async id => {
    const response = await http.get(`posts/${id}`);
    return response;
}

const create = post => {
    const data = {
        "userId": post.userId,
        "id": post.id,
        "title": post.title,
        "body": post.body
    };
    console.log('Post created')
    return http.post("/posts", data);
}

const editById = (id, post) => {
    const data = {
        title: post.title,
        body: post.body
    };
    console.log('Post edited')
    return http.put(`posts/${id}`, data);
}

const deleteById = id => {
    console.log('Post deleted')
    return http.delete(`posts/${id}`);
}

export {
    getAll,
    getById,
    create,
    editById,
    deleteById
};