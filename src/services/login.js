import axios from 'axios';

const login = (email, password) => {
    const URL = 'http://challenge-react.alkemy.org/';
    axios.post(URL, {
        email: email,
        password: password
    }, {
        headers: {
            "Content-Type": "application/json",
        },
    })
    .then((response) => {
        console.log(response);
        localStorage.setItem('blog_token', response.data.token);
    })
    .catch((error) => {
        console.log(error);
        alert("Incorrect password.")
    });
}

const logout = () => {
    localStorage.removeItem('blog_token');
}

export {
    login,
    logout
}