import React, { useEffect, useState } from 'react';
import './App.css';
import NavBar from './components/NavBar';
import Home from './components/Home';
import PostDetailContainer from './components/detail/PostDetailContainer';
import EditPostContainer from './components/edit/EditPostContainer';
import SearchIdForm from './components/edit/SearchIdForm';
import CreatePost from './components/create/CreatePost'
import Footer from './components/Footer';
import LoginForm from './components/LoginForm';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  const [logged, setLogged] = useState(false);
  const token = localStorage.getItem('blog_token')
  useEffect(() => {
    if(token === "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiZW1haWwiOiJjaGFsbGVuZ2VAYWxrZW15Lm9yZyIsImlhdCI6MTUxNjIzOTAyMn0.ilhFPrG0y7olRHifbjvcMOlH7q2YwlegT0f4aSbryBE"){
      setLogged(true);
    }
  }, [token])

  return (
    <Router>
      {
        logged ?
        <>
        <NavBar />
        <Switch>
          <Route exact path='/' component={ Home }/>
          <Route exact path='/detail/:id' component={ PostDetailContainer }/>
          <Route exact path='/edit/:id' component={ EditPostContainer }/>
          <Route exact path='/edit/' component={ SearchIdForm }/>
          <Route exact path='/create/' component={ CreatePost }/>
        </Switch>
        <Footer />
        </>
        :
        <LoginForm />
      }
    </Router>
  );
}

export default App;
